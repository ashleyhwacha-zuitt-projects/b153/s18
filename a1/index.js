// trainer object
let trainer = { 
  name: "Ashley",
  age : 25,
  pokemon : ['high', 'point', 'game'],
  friends: {

        sam: ['manila', 'boston'],
       	barry: ['austin', 'texas']  
  },
  talk: function(){
  	console.log("Pikachu! I choose you!");
  }

};

// printing trainer object
console.log(trainer);

// accessing trainer name
 let newName = trainer.name ;
 console.log(newName) ;


//invoking talk method on trainer object
 trainer.talk();